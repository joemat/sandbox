
This is a sandbox project I use to learn and try features of Java, build tools and libraries.

I use it also as a kind of notebook to remember how things work.

## Gradle build

```console
gradle wrapper
./gradlew build
```

### TODO

* Create *Component.xml* files via gradle
* Run tests via gradle
